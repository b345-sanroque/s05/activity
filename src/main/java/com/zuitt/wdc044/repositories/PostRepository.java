package com.zuitt.wdc044.repositories;

import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

//@Repository - interface that has marked of @Repository contains methods for the database manipulation.
//by inheriting CrudRepository, PostRepository has inherited its pre-defined methods for creating, retrieving, updating and deleting records, We can actually perform CRUD operations.
public interface PostRepository extends CrudRepository<Post, Object> {

    //s05 activity
    User findByUser(String user);
}
