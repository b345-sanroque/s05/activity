package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.repositories.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.zuitt.wdc044.config.JwtToken;

import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.UserRepository;

import java.util.Optional;

//to indicate this class is a service
@Service
public class PostServiceImpl implements PostService {
    @Autowired
    private PostRepository postRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    JwtToken jwtToken;

    public void createPost(String stringToken, Post post){
        String username = jwtToken.getUsernameFromToken(stringToken);
        User author = userRepository.findByUsername(username);

        Post newPost = new Post(post.getTitle(), post.getContent(), author);

        postRepository.save(newPost);
            //nasa postRepo yung CRUD opes, .save()
    }

    public Iterable<Post> getPosts(){
        //this method is from CrudRepository wherein it will contain or get all the records inside its table.
        return postRepository.findAll();
    }

    public ResponseEntity<?> updatePost(Long id, String stringToken, Post post){
        //we try to capture the username of the author who posted this specific post
        Post postForUpdating = postRepository.findById(id).get();
        String postAuthor = postForUpdating.getUser().getUsername();

        //we are going to get the username of the user who own the provided token
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        //to check
        if(authenticatedUser.equals(postAuthor)){
            postForUpdating.setTitle(post.getTitle());
            postForUpdating.setContent(post.getContent());
            //to save to the database
            postRepository.save(postForUpdating);

            return new ResponseEntity<>("Post updated successfully.", HttpStatus.OK);
        } else{
            return new ResponseEntity<>("You are not authorized to edit this post.", HttpStatus.UNAUTHORIZED);
        }
    }

    //implementation of the deletePost action from the PostService
    public ResponseEntity<?> deletePost(Long id, String stringToken){

        //variable checking, if out of range
        Optional<Post> postForDeleting2 = postRepository.findById(id);

        //if post is not existing
        if(postForDeleting2.isEmpty()){
            return new ResponseEntity<>("Post cannot be found.", HttpStatus.BAD_REQUEST);
        } else{
            //we need to find post to delete and author that created the post
            Post postForDeleting = postRepository.findById(id).get();
            String postAuthor = postForDeleting.getUser().getUsername();

            //token
            String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

            //checking
            if(authenticatedUser.equals(postAuthor)){
                //from CrudRepository
                postRepository.deleteById(id);
                return new ResponseEntity<>("Post deleted successfully.", HttpStatus.OK);
            } else{
                return new ResponseEntity<>("You are not authorized to delete this post.", HttpStatus.UNAUTHORIZED);
            }
        }
    }


    //s05 activity
    /*
    Create a feature for retrieving a list of all a user's posts. This feature needs a controller method run by a GET request to the /myPosts endpoint. It should call another method in the service implementation, and passes a string token from the user's JWT.
    */
    public Iterable<Post> getMyPosts(String stringToken) {
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);
        User user = userRepository.findByUsername(authenticatedUser);

        return user.getPosts();
    }

}

/*
DOCUMENTATION:
    https://docs.spring.io/spring-data/commons/docs/current/api/org/springframework/data/repository/CrudRepository.html
*/
