package com.zuitt.wdc044.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "users")
public class User {

    //1.PROPERTIES
    @Id
    @GeneratedValue
    private long id;
    @Column
    private String username;
    @Column
    private String password;

    //add another property
    //Set<> - is a collection where elements should not be duplicated
    @OneToMany(mappedBy = "user") //ONE user can have MANY post
    @JsonIgnore
    public Set<Post> posts;

    //2.CONSTRUCTORS
    public User(){}

    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    //3.GETTERS & SETTERS
    public String getUsername(){
        return this.username;
    }
    public String getPassword(){
        return this.password;
    }
    public void setUsername(String firstName){
        this.username = username;
    }
    public void setPassword(String lastName){
        this.password = password;
    }

    public Set<Post> getPosts(){
        return this.posts;
    }

    //4.METHODS
    public long getId() {
        return id;
    }
}
